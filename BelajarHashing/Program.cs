﻿using System;

namespace BelajarHashing
{
    class Program
    {
        static void Main(string[] args)
        {
            var userPassword = "HelloWorld!";
            var hash = BCrypt.Net.BCrypt.HashPassword(userPassword, 12);

            Console.WriteLine(hash);

            var valid = BCrypt.Net.BCrypt.Verify(userPassword, hash);
            Console.WriteLine(valid);
        }
    }
}
