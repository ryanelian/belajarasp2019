﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BelajarAsp.Services
{
    public class Product
    {
        public Guid ProductID { set; get; }

        public string Name { set; get; }

        public string Description { set; get; }
    }

    public class ProductDatabaseService
    {
        public List<Product> Products { set; get; } = new List<Product>();
    }
}
