﻿using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BelajarAsp.Services
{
    public class ProductService
    {
        private readonly IDistributedCache Cache;

        public ProductService(IDistributedCache distributedCache)
        {
            Cache = distributedCache;
        }

        public async Task CreateProductAsync(Product product)
        {
            var products = await GetProductsAsync();
            if (products == null)
            {
                products = new List<Product>();
            }

            products.Add(product);
            var productsJson = JsonConvert.SerializeObject(products);
            await Cache.SetStringAsync("Products", productsJson);
        }

        public async Task<List<Product>> GetProductsAsync()
        {
            var valueJson = await Cache.GetStringAsync("Products");
            if (valueJson == null)
            {
                return new List<Product>();
            }
            return JsonConvert.DeserializeObject<List<Product>>(valueJson);
        }

        public async Task UpdateProductsAsync(List<Product> products)
        {
            var json = JsonConvert.SerializeObject(products);
            await Cache.SetStringAsync("Products", json);
        }
    }
}
