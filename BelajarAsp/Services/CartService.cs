﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BelajarAsp.Services
{
    public class CartItem
    {
        public Guid ProductID { set; get; }

        public int Qty { set; get; }
    }

    public class CartService
    {
        private readonly IHttpContextAccessor HttpContext;

        public CartService(IHttpContextAccessor httpContextAccessor)
        {
            this.HttpContext = httpContextAccessor;
        }

        private readonly object _CartSynchronizer = new object();

        private List<CartItem> _Cart;

        public List<CartItem> Cart
        {
            get
            {
                // Cara bikin singleton :)
                // Lazy Loading Pattern
                if (_Cart == null)
                {
                    lock (_CartSynchronizer)
                    {
                        if (_Cart == null)
                        {
                            var cartJSON = HttpContext.HttpContext.Session.GetString("Cart");
                            if (string.IsNullOrEmpty(cartJSON) == false)
                            {
                                _Cart = JsonConvert.DeserializeObject<List<CartItem>>(cartJSON);
                            }
                            else
                            {
                                _Cart = new List<CartItem>();
                            }
                        }
                    }
                }
                return _Cart;
            }
            set
            {
                var cartJSON = JsonConvert.SerializeObject(value);
                HttpContext.HttpContext.Session.SetString("Cart", cartJSON);
            }
        }

        public async Task UpdateCart(CartItem item)
        {
            await HttpContext.HttpContext.Session.LoadAsync(); // ya sebenernya bukan disini sih, tapi sebenernya pas appnya jalan (middleware biasanya)

            var cart = Cart;
            var update = cart.Where(Q => Q.ProductID == item.ProductID).FirstOrDefault();
            if (update != null)
            {
                update.Qty = item.Qty;
            }
            else if (item.Qty <= 0)
            {
                cart.Remove(update);
            }
            else
            {
                cart.Add(item);
            }

            Cart = cart;
        }
    }
}
