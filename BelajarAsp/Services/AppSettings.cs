﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BelajarAsp.Services
{
    public class AppSettings
    {
        public class ASubOption
        {
            public string B { set; get; }
        }

        public string Foo { set; get; }

        public int Dua { set; get; }

        public ASubOption A { set; get; }
    }
}
