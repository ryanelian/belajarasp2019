using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BelajarAsp.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using StackExchange.Redis;
using Swashbuckle.AspNetCore.Swagger;

namespace BelajarAsp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                //options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services
                .AddAuthentication("BelajarLogin")
                .AddCookie("BelajarLogin", option =>
                {
                    option.LoginPath = "/auth/login";
                    option.LogoutPath = "/auth/logout";
                    option.AccessDeniedPath = "/auth/denied";
                    option.SessionStore = new RedisTicketStore(new Microsoft.Extensions.Caching.StackExchangeRedis.RedisCacheOptions
                    {
                        Configuration = "localhost",
                        InstanceName = "BelajarAspLogin"
                    });
                });

            //// services.AddTransient<BelajarService>();
            //services.AddScoped<BelajarService>();
            //// services.AddSingleton<BelajarService>();
            //services.AddScoped<Belajar2Service>();

            services.AddSingleton<ProductDatabaseService>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Belajar API", Version = "v1" });
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.Configure<AppSettings>(Configuration);
            services.AddScoped(di => di.GetRequiredService<IOptionsMonitor<AppSettings>>().CurrentValue);

            services.AddScoped<CartService>();
            services.AddScoped<ProductService>();

            //services.AddDistributedMemoryCache();
            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = "localhost";
                options.InstanceName = "BelajarAsp";
            });

            var cm = ConnectionMultiplexer.Connect("localhost");
            services.AddDataProtection().PersistKeysToStackExchangeRedis(cm, "BelajarAspLoginEncryptionKey");

            services.AddSession();
            services.AddHttpContextAccessor();
            services.AddHttpClient();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseSession();

            app.UseAuthentication();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Belajar API");
                c.DisplayOperationId();
            });

            app.UseMvc();
        }
    }
}
