﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using BelajarAsp.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BelajarAsp.API
{
    public class ProductListItem
    {
        public Guid ProductID { set; get; }

        public string Name { set; get; }
    }

    public class ProductDetails
    {
        public Guid ProductID { set; get; }

        public string Name { set; get; }

        public decimal Rating { set; get; }

        public string Description { set; get; }
    }

    public class ProductCreateModel
    {
        [Required]
        [StringLength(255)]
        public string Name { set; get; }

        [Required]
        [StringLength(2000)]
        public string Description { set; get; }
    }

    [Route("api/value")]
    [ApiController]
    [Produces("application/json")]
    public class ValuesApiController : ControllerBase
    {
        private readonly ProductDatabaseService DB;

        public ValuesApiController(ProductDatabaseService productDatabaseService)
        {
            this.DB = productDatabaseService;
        }

        /// <summary>
        /// /api/value balikin list of items
        /// </summary>
        /// <returns></returns>
        [HttpGet(Name = "GetAllProducts")]
        public ActionResult<List<Product>> Get(string query)
        {
            //OnPost OnGet
            //var products = new List<ProductListItem>();
            //products.Add(new ProductListItem
            //{
            //    Name = "Panadol",
            //    ProductID = Guid.NewGuid()
            //});
            //products.Add(new ProductListItem
            //{
            //    Name = "Paramex",
            //    ProductID = Guid.NewGuid()
            //});

            //if (string.IsNullOrEmpty(query) == false)
            //{
            //    products = products.Where(Q => Q.Name == query).ToList();
            //}

            var result = DB.Products.AsEnumerable();

            if (string.IsNullOrEmpty(query) == false)
            {
                result = result.Where(Q => Q.Name.Contains(query, StringComparison.OrdinalIgnoreCase));
            }

            return result.ToList();
            //return products;
        }

        [HttpGet("{id:Guid}", Name = "GetProductDetails")]
        public ActionResult<ProductDetails> Get(Guid id)
        {
            // cari product dengan id sekian dari database
            // eh gak ketemu...
            var gakKetemu = false;
            if (gakKetemu)
            {
                return NotFound("Gak ketemu");
            }

            return new ProductDetails
            {
                Name = "Panadol",
                Description = "Obat sakit kepala",
                ProductID = Guid.NewGuid(),
                Rating = 4.4M
            };
        }

        [HttpPost("{id}", Name = "UpdateProduct")]
        public ActionResult<bool> Update(Guid id, [FromBody]ProductCreateModel model)
        {
            var product = DB
               .Products
               .Where(Q => Q.ProductID == id)
               .FirstOrDefault();

            if (product == null)
            {
                return NotFound("Gak ada!");
            }

            // baru elo update :)
            product.Description = model.Description;
            product.Name = model.Name;
            // save change
            return true;
        }

        [HttpPost(Name = "CreateNewProduct")]
        public ActionResult<bool> Post([FromBody]ProductCreateModel model)
        {
            //if (ModelState.IsValid == false)
            //{
            //    return BadRequest(ModelState);
            //}

            // masukin data ke database...
            DB.Products.Add(new Product
            {
                Description = model.Description,
                Name = model.Name,
                ProductID = Guid.NewGuid()
            });

            return true;
        }

        [HttpDelete("{id:Guid}", Name = "DeleteProduct")]
        public ActionResult<bool> Delete(Guid id)
        {
            var product = DB
                .Products
                .Where(Q => Q.ProductID == id)
                .FirstOrDefault();

            if (product == null)
            {
                return NotFound("Gak ada!");
            }

            DB.Products.Remove(product);
            return true;
        }
    }
}