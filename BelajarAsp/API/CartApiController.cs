﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BelajarAsp.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BelajarAsp.API
{
    [Route("api/cart")]
    [ApiController]
    public class CartApiController : ControllerBase
    {
        private readonly CartService CartMan;

        public CartApiController(CartService cartService)
        {
            this.CartMan = cartService;
        }
        
        [HttpGet(Name = "ReadCart")]
        public async Task<ActionResult<List<CartItem>>> GetAsync()
        {
            await HttpContext.Session.LoadAsync();
            return CartMan.Cart;
        }

        [HttpPost(Name = "WriteCart")]
        public async Task<ActionResult<bool>> PostAsync([FromBody]CartItem value)
        {
            await CartMan.UpdateCart(value);
            return true;
        }
    }
}
