﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;

namespace BelajarAsp.API
{
    [Route("api/cache")]
    [ApiController]
    [Produces("application/json")]
    public class CacheApiController : ControllerBase
    {
        private readonly IDistributedCache Cache;

        public CacheApiController(IDistributedCache distributedCache)
        {
            this.Cache = distributedCache;
        }

        [HttpGet(Name = "ReadCache")]
        public async Task<ActionResult<string>> GetAsync()
        {
            //await HttpContext.Session.LoadAsync();
            //var value = HttpContext.Session.GetString("Nama");
            var value = await Cache.GetStringAsync("Nama");
            return value;
        }

        [HttpPost(Name = "WriteCache")]
        public async Task<ActionResult<bool>> PostAsync([FromBody]string value)
        {
            //await HttpContext.Session.LoadAsync();
            //HttpContext.Session.SetString("Nama", value);
            await Cache.SetStringAsync("Nama", value, new DistributedCacheEntryOptions
            {
                AbsoluteExpiration = DateTimeOffset.UtcNow.AddMinutes(2)
            });
            return true;
        }
    }
}