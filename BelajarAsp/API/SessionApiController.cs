﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BelajarAsp.API
{
    [Route("api/session")]
    [ApiController]
    [Produces("application/json")]
    public class SessionApiController : ControllerBase
    {
        [HttpGet(Name = "ReadSession")]
        public async Task<ActionResult<string>> GetAsync()
        {
            await HttpContext.Session.LoadAsync();
            var value = HttpContext.Session.GetString("Nama");
            return value;
        }

        [HttpPost(Name = "WriteSession")]
        public async Task<ActionResult<bool>> PostAsync([FromBody]string value)
        {
            await HttpContext.Session.LoadAsync();
            HttpContext.Session.SetString("Nama", value);
            return true;
        }
    }
}
