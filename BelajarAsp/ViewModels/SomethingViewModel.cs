﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BelajarAsp.ViewModels
{
    public class SomethingViewModel
    {
        [Required]
        public string Name { set; get; }
    }
}
