using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BelajarAsp.Pages
{
    // Owner AND (Finance OR IT)
    [Authorize(Roles = "Owner")]
    [Authorize(Roles = "Finance,IT")]
    public class WelcomeModel : PageModel
    {
        [BindProperty(SupportsGet = true)]
        public string Name { get; set; }

        public IActionResult OnGet(/*string name*/)
        {
            //if (User.IsInRole("Administrator"))
            //{
            //    // tendang?
            //}
            //return RedirectToPage("./Apalah/Index");
            //this.Name = name;
            return Page();
        }
    }
}