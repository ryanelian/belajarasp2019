using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BelajarAsp.API;
using BelajarAsp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BelajarAsp.Pages.Product
{
    public class IndexModel : PageModel
    {
        private readonly ProductService ProductMan;
        private readonly IHttpClientFactory HttpFac;

        public IndexModel(ProductService productService, IHttpClientFactory httpClientFactory)
        {
            this.ProductMan = productService;
            this.HttpFac = httpClientFactory;
        }

        [BindProperty]
        public List<Services.Product> Items { set; get; }

        public decimal Quantity;

        public async Task OnGetAsync()
        {
            Items = await ProductMan.GetProductsAsync();
        }

        public async Task<IActionResult> OnPostAsync(Guid id, int qty)
        {
            var client = HttpFac.CreateClient();
            // GAK BAKAL JALAN KARENA HTTPCLIENT SESSION =/= BROWSER SESSION :(
            var response = await client.PostAsJsonAsync("http://localhost:58595/api/cart", new CartItem
            {
                ProductID = id,
                Qty = qty
            });

            if (response.IsSuccessStatusCode == false)
            {
                throw new Exception("Cart API Error: Failed");
            }

            return RedirectToPage();
        }

    }
}