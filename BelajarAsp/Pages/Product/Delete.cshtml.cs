using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BelajarAsp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BelajarAsp.Pages.Product
{
    public class DeleteModel : PageModel
    {
        private readonly ProductService ProductMan;

        [BindProperty(SupportsGet =true)]
        public Guid ID { get; set; }

        [BindProperty]
        public string Nama { get; set; }

        public DeleteModel(ProductService productDatabaseService)
        {
            this.ProductMan = productDatabaseService;
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var items = await ProductMan.GetProductsAsync();
            var product = items
                .Where(Q => Q.ProductID == ID)
                .FirstOrDefault();

            if (product == null)
            {
                return NotFound();
            }
            Nama = product.Name;
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var items = await ProductMan.GetProductsAsync();
            var product = items
                .Where(Q => Q.ProductID == ID)
                .FirstOrDefault();

            if (product == null)
            {
                return NotFound();
            }

            items.Remove(product);
            await ProductMan.UpdateProductsAsync(items);
            return RedirectToPage("./Index");
        }
    }
}