using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using BelajarAsp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BelajarAsp.Pages.Product
{
    public class CreateModel : PageModel
    {
        private readonly ProductService ProductMan;

        public CreateModel(ProductService productService)
        {
            this.ProductMan = productService;
        }
        
        public class CreateProductFormModel
        {
            [Required]
            [StringLength(255)]
            public string Name { set; get; }

            [Required]
            [StringLength(255)]
            public string Description { set; get; }
        }

        [BindProperty]
        public CreateProductFormModel Form { set; get; }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid == false)
            {
                return Page();
            }

            await ProductMan.CreateProductAsync(new Services.Product
            {
                ProductID = Guid.NewGuid(),
                Name = Form.Name,
                Description = Form.Description
            });
            
            return RedirectToPage();
        }
    }
}