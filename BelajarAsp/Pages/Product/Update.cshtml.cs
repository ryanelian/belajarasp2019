using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using BelajarAsp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BelajarAsp.Pages.Product
{
    public class UpdateModel : PageModel
    {
        private readonly ProductService ProductMan;

        public UpdateModel(ProductService productService)
        {
            this.ProductMan = productService;
        }

        public class UpdateProductFormModel
        {
            [Required]
            [StringLength(25, MinimumLength = 8)]
            public string NameUpdate { set; get; }

            [Required]
            [StringLength(300, MinimumLength = 35)]
            public string DescriptionUpdate { set; get; }
        }

        [BindProperty]
        public UpdateProductFormModel FormUpdate { get; set; }

        [BindProperty(SupportsGet = true)]
        public Guid ID { set; get; }

        public async Task<IActionResult> OnGetAsync()
        {
            var items = await ProductMan.GetProductsAsync();
            var product = items
                .Where(Q => Q.ProductID == ID)
                .FirstOrDefault();

            if (product == null)
            {
                return NotFound();
            }

            FormUpdate = new UpdateProductFormModel
            {
                NameUpdate = product.Name,
                DescriptionUpdate = product.Description
            };
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var items = await ProductMan.GetProductsAsync();
            var product = items
                .Where(Q => Q.ProductID == ID)
                .FirstOrDefault();
            
            if (product == null)
            {
                return NotFound();
            }

            if (ModelState.IsValid == false)
            {
                return Page();
            }

            product.Name = FormUpdate.NameUpdate;
            product.Description = FormUpdate.DescriptionUpdate;
            await ProductMan.UpdateProductsAsync(items);

            return RedirectToPage();
        }

    }
}