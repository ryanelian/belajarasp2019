using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;

namespace BelajarAsp.Pages.Apalah
{
    public class TestModel : PageModel
    {
        //private readonly IConfiguration Configuration;

        [BindProperty]
        [Required]
        public string Name { set; get; }

        [TempData]
        public string SuccessMessage { get; set; }

        public string Foo { get; private set; }

        public int Dua { get; private set; }

        //public TestModel(IConfiguration configuration)
        //{
        //    this.Configuration = configuration;
        //}

        public void OnGet([FromServices]IConfiguration configuration)
        {
            this.Foo = configuration["Foo"];
            this.Dua = configuration.GetValue<int>("Dua");
        }

        public IActionResult OnPost()
        {
            if (ModelState.IsValid == false)
            {
                return Page();
            }

            // proses data...
            // masukin kemana kek

            SuccessMessage = "Berhasil hore!";
            TempData["ErrorMessage"] = "Sesuatu terjadi!";
            return RedirectToPage("./Index");
        }
    }
}