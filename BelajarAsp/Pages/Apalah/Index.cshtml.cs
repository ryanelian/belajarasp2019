using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BelajarAsp.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BelajarAsp.Pages.Apalah
{
    public class IndexModel : PageModel
    {
        private readonly AppSettings Settings;

        public IndexModel(AppSettings appSettings)
        {
            this.Settings = appSettings;
        }

        public string Foo { get; private set; }

        public IActionResult OnGet()
        {
            //throw new Exception("MELEDAK")
            this.Foo = Settings.Foo;
            return Page();
        }
    }
}