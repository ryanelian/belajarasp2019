using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using BelajarAsp.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BelajarAsp.Pages
{
    [Authorize]
    public class IndexModel : PageModel
    {
        //private readonly BelajarService Belajar;
        //private readonly Belajar2Service Belajar2;

        //public IndexModel(BelajarService belajarService, Belajar2Service belajar2Service)
        //{
        //    this.Belajar = belajarService;
        //    this.Belajar2 = belajar2Service;
        //}

        public class LoginFormModel
        {
            [Required]
            [StringLength(255)]
            [Display(Name = "Alamat Surat Elektronik")]
            public string Email { set; get; }

            [Required(ErrorMessage = "Kosong woi")]
            [StringLength(maximumLength: 64, MinimumLength = 8)]
            public string Password { set; get; }
        }

        [BindProperty]
        public LoginFormModel Form { set; get; }

        [BindProperty]
        public string Name { set; get; }

        public bool GetYah { get; private set; }

        public void OnGet()
        {
            //var x = Belajar2.AddKali2(1, 2);

            this.GetYah = true;
        }

        public IActionResult OnPost()
        {
            //this.Namamu = name;
            // PRG: Post-Redirect-Get
            if (ModelState.IsValid == false)
            {
                return Page();
            }

            if (Form.Password.All(Q => char.IsLower(Q)))
            {
                ModelState.AddModelError("Form.Password", "gedein dikit napa");
                return Page();
            }

            // Masukin data ke database...
            var a = Form.Email;
            var b = Form.Password;
            return RedirectToPage("./Welcome", new
            {
                Name = Name
            });
        }

        //public string Namamu { get; private set; }

    }
}